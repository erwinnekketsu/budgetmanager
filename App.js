import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import useLinking from './src/navigation/useLinking';
import {  Splash } from "./src/screens/Splash";
import { RootStackScreen} from './src/navigation/RouteStack';
import { AuthContext } from './context';

export default function App(props) {
    const [initialNavigationState, setInitialNavigationState] = React.useState();
    const [isLoading, setIsLoading] = React.useState(true);
    const [userToken, setUserToken] = React.useState(null);
    const containerRef = React.useRef();
    const { getInitialState } = useLinking(containerRef);

    const authContext = React.useMemo(() => {
        return {
        Login: () => {
            setIsLoading(false);
            setUserToken("asdf");
        },
        Register: () => {
            setIsLoading(false);
            setUserToken("asdf");
        },
        signOut: () => {
            setIsLoading(false);
            setUserToken(null);
        }
        };
    }, []);

    // Load any resources or data that we need prior to rendering the app
    React.useEffect(() => {
        async function loadResourcesAndDataAsync() {
        try {
            SplashScreen.preventAutoHide();

            // Load our initial navigation state
            setInitialNavigationState(await getInitialState());

            // Load fonts
            await Font.loadAsync({
            ...Ionicons.font,
            'space-mono': require('./src/assets/fonts/SpaceMono-Regular.ttf'),
            });
        } catch (e) {
            // We might want to provide this error information to an error reporting service
            console.warn(e);
        } finally {
            setIsLoading(true);
            SplashScreen.hide();
        }
        }

        loadResourcesAndDataAsync();
        setTimeout(() => {
            setIsLoading(false);
        }, 1000);
    }, []);

    
    if (isLoading) {
        return <Splash />;
    }

    return (
        <AuthContext.Provider value={authContext}>
            <View style={styles.container}>
                {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
                    <RootStackScreen userToken={userToken} />
                </NavigationContainer>
            </View>
        </AuthContext.Provider>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
