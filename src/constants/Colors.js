const tintColor = 'seagreen';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  primaryColor: "#1cda93",
  secondaryColor: "#3FA742",
  terciaryColor: "#bdc3c7",
  polylineColor: "#e74c3c",
  statusBarColor: "#212121",
  lightBackgroundColor: "white",
  darkBackgroundColor: "black",
  redColor: "#ff5a50",
  lightText: "#5A5A5A",
  lighterText: "white",
  orangeColor: "#fff200",
  blueColor: "#17c0eb",
  darkText: "#4d4d4d",
  darkerText: "black",
  grayColor: "#ecf0f1",

  errorColor: "#d34651"
};
