import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {  Login } from "../screens/Login";
import {  Register } from "../screens/Register";

const AuthStack = createStackNavigator();
export const AuthStackScreen = () => (
    <AuthStack.Navigator>
      <AuthStack.Screen
        name="Login"
        component={Login}
        options={{ title: ' ', headerShown: false }}
      />
      <AuthStack.Screen
        name="Register"
        component={Register}
        options={{ title: "Register", headerShown: false }}
      />
    </AuthStack.Navigator>
);