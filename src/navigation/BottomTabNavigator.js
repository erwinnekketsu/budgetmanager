import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as React from 'react';

import TabBarIcon from '../components/TabBarIcon';
import Home from '../screens/Home/index';
import Calendar from '../screens/CalendarScreen/index';
import Report from '../screens/Report/index';
import Wallet from '../screens/Wallet/index';
import Setting from '../screens/Setting/index';

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = '';

export default function BottomTabNavigator({ navigation, route }) {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  navigation.setOptions({ headerTitle: getHeaderTitle(route) });

  return (
    <BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME}>
      <BottomTab.Screen
        name="Home"
        component={Home}
        options={{
          title: ' ',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="swap" />,
        }}
      />
      <BottomTab.Screen
        name="Links"
        component={Calendar}
        options={{
          title: ' ',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="calendar" />,
        }}
      />
      <BottomTab.Screen
        name="Report"
        component={Report}
        options={{
          title: ' ',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="piechart" />,
        }}
      />
      <BottomTab.Screen
        name="Wallet"
        component={Wallet}
        options={{
          title: ' ',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="wallet" />,
        }}
      />
      <BottomTab.Screen
        name="Setting"
        component={Setting}
        options={{
          title: ' ',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="setting" />,
        }}
      />
    </BottomTab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {
    case 'Home':
      return 'How to get started';
    case 'Links':
      return 'Links to learn more';
    case 'Report':
      return 'Report';
    case 'Wallet':
      return 'Wallet';
    case 'Setting':
      return 'Setting';
    case 'Register':
      return 'Register';
  }
}
