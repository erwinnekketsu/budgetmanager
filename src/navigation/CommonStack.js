import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Account } from "../screens/Setting/account";
import BackButton from "../components/BackButton";

const CommonStack = createStackNavigator();
export const CommonStackScreen = ({ navigation }) => (
    <CommonStack.Navigator>
      <CommonStack.Screen
        name="Account"
        component={Account}
        options={{ 
            title: "Account",
            headerLeft: () => (
                <BackButton
                    icon="ios-arrow-back"
                    label=""
                    onPress={() => navigation.navigate('Home', { screen: 'Setting' })}
                />
            ),
         }}
      />
    </CommonStack.Navigator>
);