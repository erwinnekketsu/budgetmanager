import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import BottomTabNavigator from './BottomTabNavigator';
import { CommonStackScreen } from './CommonStack';

const Drawer = createDrawerNavigator();
export const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Profile">
    <Drawer.Screen name="Home" component={BottomTabNavigator} />
    <Drawer.Screen name="Common" component={CommonStackScreen}/>
  </Drawer.Navigator>
);