import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { DrawerScreen } from './DrawerStack';
import { AuthStackScreen } from './AuthStack';

const RootStack = createStackNavigator();
export const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode="none">
    {userToken ? (
      <RootStack.Screen
        name="Root"
        component={DrawerScreen}
      />
    ) : (
      <RootStack.Screen
        name="Auth"
        component={AuthStackScreen}
      />
    )}
  </RootStack.Navigator>
);