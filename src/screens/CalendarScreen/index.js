import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import {Calendar} from 'react-native-calendars';

const config = require('./config');

export default function CalendarScreen() {
  const [selected, setSelected] = React.useState(null)
  const onDayPress = day => {
    setSelected(day.dateString)
  }

  const dateNow = new Date();
  
  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
      <Calendar
        testID={config.calendars.FIRST}
        current={{dateNow: {
          customStyles: {
            text: {
              color: 'green'
            }
          }
        } }}
        theme={{
          todayTextColor: "white",
          todayBackgroundColor: "seagreen",
        }}
        style={styles.calendar}
        hideExtraDays
        onDayPress={onDayPress}
        markedDates={{
          [selected]: {
            selected: true, 
            disableTouchEvent: true,  
            selectedColor: 'seagreen',
            selectedDotColor: 'seagreen',
          }
        }}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingTop: 20,
  },
  optionIconContainer: {
    marginRight: 12,
  },
  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: 0,
    borderColor: '#ededed',
  },
  lastOption: {
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  optionText: {
    fontSize: 15,
    alignSelf: 'flex-start',
    marginTop: 1,
  },
  calendar: {
    marginTop: 15,
    marginBottom: 10,
    borderRadius: 10,
    shadowColor: 'grey',
    shadowOffset: {
        width: 0,
        height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  text: {
    textAlign: 'center',
    padding: 10,
    backgroundColor: 'lightgrey',
    fontSize: 16
  }
});
