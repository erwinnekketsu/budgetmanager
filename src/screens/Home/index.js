import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { Image, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { styles } from './styles';
import { REACT_APP_API_URL_HOST } from 'react-native-dotenv';
import Moment from 'react-moment';
import {months, days} from '../../constants/mocks'

export default function Home() {
  const [balanceHistory, setBalanceHistory] = React.useState([]);
  const [balance, setBalance] = React.useState('');
  const [income, setIncome] = React.useState('');
  const [expense, setExpense] = React.useState('');

  const token = localStorage.getItem('token')

  var formatter = new Intl.NumberFormat('id-ID', {
    maximumSignificantDigits: 1,
    style: 'currency',
    currency: 'IDR',
  });

  React.useEffect(() => {
    fetch(
      `${REACT_APP_API_URL_HOST}balance`,
      {
        method: "GET",
        headers: new Headers({
          Authorization: token
        })
      }
    )
      .then(res => res.json())
      .then(response => {
        setBalance(response.data.balance);
        setIncome(response.data.income);
        setExpense(response.data.expense);
      })
      .catch(error => console.log(error));

    fetch(
      `${REACT_APP_API_URL_HOST}balance_histories`,
      {
        method: "GET",
        headers: new Headers({
          Authorization: token
        })
      }
    )
      .then(res => res.json())
      .then(response => {
        setBalanceHistory(response.data);
      })
      .catch(error => console.log(error));
  }, [balance]);
  return (
      <View style={styles.container}>
        <View style={styles.card}>
          <View style={styles.cardDivLeft}>
            <Text style={styles.cardTextDivLeft}>Balance</Text>
            <Text style={styles.cardTextLeft}>Rp. {balance}</Text>
          </View>
          <View style={styles.cardDivRight}>
            <View style={styles.cardDivRightIncome}>
              <Text style={styles.cardTextDivRight}>Income</Text>
              <Text style={styles.cardTextRight}>Rp. {income}</Text>
            </View>
            <View style={styles.cardDivRightExpense}>
              <Text style={styles.cardTextDivRight}>Expense</Text>
              <Text style={styles.cardTextRight}>Rp. {expense}</Text>
            </View>
          </View>
        </View>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        {balanceHistory.map(balanceHis => (
          <>
            <View style={styles.cardList}>
              <View style={styles.cardListHeader}>
                <View style={styles.cardListDate}>
                  <Text style={styles.cardListDateText}><Moment format="DD">{balanceHis.date}</Moment></Text>
                </View>
                <View style={styles.cardListDay}>
                  <Text style={styles.cardListDayTextTop}><Moment format="dddd">{balanceHis.date}</Moment></Text>
                  <Text style={styles.cardListDayTextBottom}><Moment format="MMMM Y">{balanceHis.date}</Moment></Text>
                </View>
                <View style={styles.cardListDayTotal}>
                  <Text style={styles.cardListDayTotalText}>Rp {balanceHis.histories[0].balance_daily}</Text>
                </View>
              </View>
              
              {balanceHis.histories.map(histories => (

                <View style={styles.cardListContent} key={histories.id} >
                  <View style={styles.cardListContentIcon}>
                    <Image 
                      style={styles.cardListContentIconImage}
                      source={require('../../assets/images/robot-dev.png')}
                    />
                  </View>
                  <View style={styles.cardListContentTrx}>
                    <Text style={styles.cardListDayTextTop}><Moment format="dddd">{histories.settledAt}</Moment></Text>
                    <Text style={styles.cardListDayTextBottom}><Moment format="MM Y">{histories.settledAt}</Moment></Text>
                  </View>
                  <View style={styles.cardListContentPrice}>
                    <Text style={styles.cardListDayTotalText}>Rp {histories.amount}</Text>
                  </View>
                </View>
              
            ))}
            
            </View>
          </>
        ))}
        </ScrollView>

      </View>
  );
}

Home.navigationOptions = {
  header: null,
};

function DevelopmentModeNotice() {
  if (__DEV__) {
    const learnMoreButton = (
      <Text onPress={handleLearnMorePress} style={styles.helpLinkText}>
        Learn more
      </Text>
    );

    return (
      <Text style={styles.developmentModeText}>
        Development mode is enabled: your app will be slower but you can use useful development
        tools. {learnMoreButton}
      </Text>
    );
  } else {
    return (
      <Text style={styles.developmentModeText}>
        You are not in development mode: your app will run at full speed.
      </Text>
    );
  }
}

function handleLearnMorePress() {
  WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/workflow/development-mode/');
}

function handleHelpPress() {
  WebBrowser.openBrowserAsync(
    'https://docs.expo.io/versions/latest/get-started/create-a-new-app/#making-your-first-change'
  );
}
