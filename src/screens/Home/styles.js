import { Platform, StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        fontSize: 32,
        backgroundColor: "#fff"
    },
    developmentModeText: {
        marginBottom: 20,
        color: 'rgba(0,0,0,0.4)',
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 5,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
    },
    homeFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        lineHeight: 24,
        textAlign: 'center',
    },
    tabBarInfoContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        ...Platform.select({
        ios: {
            shadowColor: 'black',
            shadowOffset: { width: 0, height: -3 },
            shadowOpacity: 0.1,
            shadowRadius: 3,
        },
            android: {
            elevation: 20,
            },
        }),
        alignItems: 'center',
        backgroundColor: '#fbfbfb',
        paddingVertical: 20,
    },
    tabBarInfoText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
    },
    navigationFilename: {
        marginTop: 5,
    },
    helpContainer: {
        marginTop: 15,
        alignItems: 'center',
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
    card: {
        height: 100,
        backgroundColor: 'seagreen',
        margin: 5,
        marginTop: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
        flexDirection: 'row',
    },
    cardDivLeft: {
        width: '50%',
        borderRightWidth: 1,
        borderRightColor: 'white',
    },
    cardDivRight: {
        width: '50%'
    },
    cardDivRightIncome: {
        height: '50%',
    },
    cardDivRightExpense: {
        height: '50%'
    },
    cardTextDivLeft: {
        color: '#fff',
        textAlign: 'left',
        marginTop: 20,
        marginLeft: 5
    },
    cardTextDivRight: {
        color: '#fff',
        textAlign: 'left',
        marginTop: 7,
        marginLeft: 5
    },
    cardTextLeft: {
        color: '#fff',
        textAlign: 'left',
        marginTop: 5,
        marginLeft: 5,
        fontSize: 20,
    },
    cardTextRight: {
        color: '#fff',
        textAlign: 'left',
        marginLeft: 5,
        fontSize: 15,
    },
    //scroll view begin
    cardList: {
        backgroundColor: '#fff',
        margin: 5,
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
    },
    cardListHeader: {
        height: 50,
        flexDirection: 'row',
    },
    cardListDate: {
        width: '20%',
        margin: 'auto',
    },
    cardListDateText: {
        fontSize: 30,
        fontWeight: "bold",
        textAlign: 'center',
        marginTop: 5,
    },
    cardListDay: {
        width: '30%',
        paddingTop: 8,
    },
    cardListDayTextTop: {
        fontSize: 15,
        fontWeight: "bold",
        textAlign: 'left',
    },
    cardListDayTexBottom: {
        fontSize: 10,
        textAlign: 'left',
    },
    cardListDayTotal: {
        width: '50%',
    },
    cardListDayTotalText: {
        fontSize: 15,
        fontWeight: "bold",
        textAlign: 'right',
        marginRight: 10,
        marginTop: 15,
    },
    cardListContent: {
        flexDirection: 'row',
    },
    cardListContentIcon: {
        width: '20%',
        margin: 'auto',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardListContentIconImage: {
        marginTop: 5,
        borderRadius: 400/ 2,
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },
    cardListContentTrx: {
        width: '30%',
        paddingTop: 8,
    },
    cardListContentPrice: {
        width: '50%',
    },
    //scroll view begin
  });