import * as React from 'react';
import { View, Text, TextInput, Image } from "react-native";
import { AuthContext } from "../../../context";
import { styles } from './styles';
import Button from './button';
import { REACT_APP_API_URL_HOST } from 'react-native-dotenv';

export const Login = ({ navigation }) => {
    const { Login } = React.useContext(AuthContext);
    const [mobile, setMobile] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [isButtonDisabled, setIsButtonDisabled] = React.useState(true);
    const [helperText, setHelperText] = React.useState('');
    const [error, setError] = React.useState(false);


    const handleLogin = () => {
        if (mobile !== '' && password !== '') {
            fetch(`${REACT_APP_API_URL_HOST}login`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    password: password,
                    mobile: mobile,
                }),
            })
            .then(response => response.json())
            .then((responseJson)=> {
                console.log("responseJson");
                console.log(responseJson);
                localStorage.setItem('token',responseJson.token)
                localStorage.setItem('dataUser',responseJson.user)
                if (responseJson.success) {
                    Login();
                } else {
                    alert(responseJson.error);
                }
            })
            .catch(error=>console.log(error)) //to catch the errors if any;
        } else if(mobile == "" && password == "") {
            alert('Please Enter Your Mobile Phone Number and Password');
        } else if(password == "") {
            alert('Please Enter Your Password');
        } else {
            alert('Please Enter Your Mobile Phone Number');
        }
    };
  
    return (
      <View style={styles.container}>
        <Image 
            style={styles.IconImage}
            source={require('../../assets/images/card.png')}
        />
        <TextInput
            style={styles.input}
            placeholder="Enter Your Mobile Phone"
            onChangeText={mobile => setMobile(mobile)}
            defaultValue={mobile}
            keyboardType={'numeric'} 
        />
        <TextInput
            style={styles.input}
            placeholder="Enter Your Password"
            onChangeText={password => setPassword(password)}
            defaultValue={password}
            secureTextEntry={true} 
        />
        <View style={styles.buttonArea}>
            <Button onPress={() => handleLogin()} >Login</Button>
            <Text style={styles.answerText}>Don't have an account yet?</Text>
            <Text style={styles.registerText} onPress={() => navigation.push("Register")}>Register</Text>
        </View>
      </View>
    );
};