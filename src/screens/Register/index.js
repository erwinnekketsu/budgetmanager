import * as React from 'react';
import { View, TextInput, Text, Image } from "react-native";
import { AuthContext } from "../../../context";
import { styles } from './styles';
import Button from './button';

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
);
export const Register = ({ navigation }) => {
    const { Register } = React.useContext(AuthContext);
    const [name, setName] = React.useState('');
    const [mobile, setMobile] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [isButtonDisabled, setIsButtonDisabled] = React.useState(true);
    const [helperText, setHelperText] = React.useState('');
    const [error, setError] = React.useState(false);
  
    return (
      <ScreenContainer>
        <Image 
            style={styles.IconImage}
            source={require('../../assets/images/card.png')}
        />
        <TextInput
            style={styles.input}
            placeholder="Input Name"
            onChangeText={name => setName(name)}
            defaultValue={name}
        />
        <TextInput
            style={styles.input}
            placeholder="Input Mobile"
            onChangeText={mobile => setMobile(mobile)}
            defaultValue={mobile}
        />
        <TextInput
            style={styles.input}
            placeholder="Input email"
            onChangeText={email => setEmail(email)}
            defaultValue={email}
        />
        <TextInput
            style={styles.input}
            placeholder="Input Password"
            onChangeText={password => setPassword(password)}
            defaultValue={password}
        />
        <View style={styles.buttonArea}>
            <Button onPress={() => Register()} >Register</Button>
            <Text style={styles.answerText}>Do have an account yet?</Text>
            <Text style={styles.registerText} onPress={() => navigation.push("Login")}>Login</Text>
        </View>
      </ScreenContainer>
    );
};
