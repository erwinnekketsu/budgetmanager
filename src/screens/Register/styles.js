import { Platform, StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    input: {
        width: 300,
        height: 44,
        padding: 10,
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
        marginBottom: 10,
    },
    buttonArea: {
        marginTop: 5
    },
    button: {
        paddingHorizontal: 25,
        paddingVertical: 12,
        backgroundColor: "red",
        marginBottom: 10,
        borderRadius: 25
    },
    IconImage: {
        marginBottom: 50,
        borderRadius: 10,
        width: 100,
        height: 100
    },
    answerText: {
        textAlign: "center",
        marginTop: 5
    },
    registerText: {
        textAlign: "center", 
        color: 'seagreen',
        fontWeight: 'bold'
    }
});