import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { Dimensions, Text, View, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { PieChart } from 'react-native-svg-charts';
import { styles } from './styles';


class Report extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      selectedSlice: {
        label: '',
        value: 0
      },
      labelWidth: 0
    }
  }
  render() {
    const { labelWidth, selectedSlice } = this.state;
    const { label, value } = selectedSlice;
    const keys = ['google', 'facebook', 'linkedin', 'youtube', 'Twitter'];
    const values = [15, 25, 35, 45, 55];
    const colors = ['#006A4E', '#2E856E', '#5CA08E', '#8ABAAE', '#B8D5CD']
    const data = keys.map((key, index) => {
        return {
          key,
          value: values[index],
          svg: { fill: colors[index] },
          arc: { outerRadius: (70 + values[index]) + '%', padAngle: label === key ? 0.1 : 0 },
          onPress: () => this.setState({ selectedSlice: { label: key, value: values[index] } })
        }
      })
    const deviceWidth = Dimensions.get('window').width

    return (
      <View style={styles.container}>
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <View style={styles.chartPie} >
          <PieChart
            style={{ height: 200 }}
            outerRadius={'80%'}
            innerRadius={'45%'}
            data={data}
          />
          <Text
            onLayout={({ nativeEvent: { layout: { width } } }) => {
              this.setState({ labelWidth: width });
            }}
            style={{
              position: 'absolute',
              left: deviceWidth / 2 - labelWidth / 2,
              textAlign: 'center',
              marginTop: 80
            }}>
            {`${label} \n ${value}`}
          </Text>
        </View>

        <View style={styles.detailChartList}>
          <View style={styles.detailChartListIcon}>
            <Image 
              style={styles.detailChartListIconImage}
              source={require('../../assets/images/robot-dev.png')}
            />
          </View>
          <View style={styles.detailChartListTrx}>
            <Text style={styles.cardListDayTextTop}>Tuesday</Text>
          </View>
          <View style={styles.detailChartListPrice}>
            <Text style={styles.cardListDayTotalText}>Rp. 1.000.000</Text>
          </View>
        </View>
      </ScrollView>
    </View>
    )
  }
}

export default Report;