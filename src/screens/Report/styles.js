import { Platform, StyleSheet } from 'react-native';    

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        paddingTop: 15,
    },
    chartStyle: {
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
    },
    chartPie: {
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
    },
    chartList: {
        margin: 'auto',
    },
    detailChartList: {
        marginTop: 10,
        backgroundColor: '#fff',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
        flexDirection: 'row',
    },
    detailChartListIcon: {
        width: '20%',
        margin: 'auto',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    detailChartListIconImage: {
        marginTop: 5,
        borderRadius: 400/ 2,
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },
    detailChartListTrx: {
        width: '30%',
        paddingTop: 20,
    },
    detailChartListPrice: {
        width: '50%',
    },
    cardListDayTextTop: {
        fontSize: 15,
        fontWeight: "bold",
        textAlign: 'left',
    },
    cardListDayTotalText: {
        fontSize: 15,
        fontWeight: "bold",
        textAlign: 'right',
        marginRight: 10,
        marginTop: 15,
    },
  });