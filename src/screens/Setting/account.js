import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
);
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    button: {
      paddingHorizontal: 20,
      paddingVertical: 10,
      marginVertical: 10,
      borderRadius: 5
    }
});
export const Account = () => {
  
    return (
      <ScreenContainer>
        <Text>Account</Text>
      </ScreenContainer>
    );
};
