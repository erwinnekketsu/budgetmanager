import React from "react";
import { View, Text, StyleSheet, Button, Image } from "react-native";

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
  );
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    IconImage: {
        marginBottom: 50,
        borderRadius: 10,
        width: 100,
        height: 100
    },
});
export const Splash = () => (
    <ScreenContainer>
        <Image 
            style={styles.IconImage}
            source={require('../../assets/images/card.png')}
        />
    </ScreenContainer>
);