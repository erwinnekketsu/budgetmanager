import { Ionicons } from '@expo/vector-icons';
import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { Image, Text, View } from 'react-native';
import { RectButton, ScrollView } from 'react-native-gesture-handler';
import { styles } from './styles'

export default function Wallet() {
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <View style={styles.cardDivLeft}>
          <Text style={styles.cardTextDivLeft}>Cash</Text>
          <Text style={styles.cardTextLeft}>Rp. 25.000.000</Text>
        </View>
        <View style={styles.cardDivRight}>
          <View style={styles.cardDivRightIncome}>
            <Button
              icon="md-add-circle-outline"
              label="Add Wallet"
              // onPress={() => WebBrowser.openBrowserAsync('https://docs.expo.io')}
            />
          </View>
        </View>
      </View>
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <View style={styles.cardList}>
          <View style={styles.cardListHeader}>
            <View style={styles.cardListDate}>
              <Text style={styles.cardListDateText}>01</Text>
            </View>
            <View style={styles.cardListDay}>
              <Text style={styles.cardListDayTextTop}>Tuesday</Text>
              <Text style={styles.cardListDayTextBottom}>July 2019</Text>
            </View>
            <View style={styles.cardListDayTotal}>
              <Text style={styles.cardListDayTotalText}>Rp. 1.000.000</Text>
            </View>
          </View>
          <View style={styles.cardListBudget}>
            <View style={styles.cardListBudgetIcon}>
              <Image 
                style={styles.cardListBudgetIconImage}
                source={require('../../assets/images/robot-dev.png')}
              />
            </View>
            <View style={styles.cardListBudgetTrx}>
              <Text style={styles.cardListDayTextTop}>Tuesday</Text>
              <Text style={styles.cardListDayTextBottom}>July 2019</Text>
            </View>
            <View style={styles.cardListBudgetPrice}>
              <Text style={styles.cardListDayTotalText}>Rp. 1.000.000</Text>
            </View>
          </View>
          <View style={styles.cardAddBudget}>
            <Button
                icon="md-add-circle-outline"
                label="Add Budget"
                // onPress={() => WebBrowser.openBrowserAsync('https://docs.expo.io')}
              />
          </View>
        </View>

        <View style={styles.cardSaving}>
          <View style={styles.cardListHeader}>
            <View style={styles.cardListDate}>
              <Text style={styles.cardListDateText}>01</Text>
            </View>
            <View style={styles.cardListDay}>
              <Text style={styles.cardListDayTextTop}>Tuesday</Text>
              <Text style={styles.cardListDayTextBottom}>July 2019</Text>
            </View>
            <View style={styles.cardListDayTotal}>
              <Text style={styles.cardListDayTotalText}>Rp. 1.000.000</Text>
            </View>
          </View>
          <View style={styles.cardListSaving}>
            <View style={styles.cardListSavingIcon}>
              <Image 
                style={styles.cardListSavingIconImage}
                source={require('../../assets/images/robot-dev.png')}
              />
            </View>
            <View style={styles.cardListSavingTrx}>
              <Text style={styles.cardListDayTextTop}>Tuesday</Text>
              <Text style={styles.cardListDayTextBottom}>July 2019</Text>
            </View>
            <View style={styles.cardListSavingPrice}>
              <Text style={styles.cardListDayTotalText}>Rp. 1.000.000</Text>
            </View>
          </View>
          <View style={styles.cardAddSaving}>
            <Button
                icon="md-add-circle-outline"
                label="Add Saving"
                // onPress={() => WebBrowser.openBrowserAsync('https://docs.expo.io')}
              />
          </View>
        </View>

        <View style={styles.cardDebt}>
          {/* <View style={styles.cardListHeader}>
            <View style={styles.cardListDate}>
              <Text style={styles.cardListDateText}>01</Text>
            </View>
            <View style={styles.cardListDay}>
              <Text style={styles.cardListDayTextTop}>Tuesday</Text>
              <Text style={styles.cardListDayTextBottom}>July 2019</Text>
            </View>
            <View style={styles.cardListDayTotal}>
              <Text style={styles.cardListDayTotalText}>Rp. 1.000.000</Text>
            </View>
          </View> */}
          {/* <View style={styles.cardListDebt}> */}
            {/* <View style={styles.cardListDebtIcon}>
              <Image 
                style={styles.cardListDebtIconImage}
                source={require('../../assets/images/robot-dev.png')}
              />
            </View>
            <View style={styles.cardListDebtTrx}>
              <Text style={styles.cardListDayTextTop}>Tuesday</Text>
              <Text style={styles.cardListDayTextBottom}>July 2019</Text>
            </View>
            <View style={styles.cardListDebtPrice}>
              <Text style={styles.cardListDayTotalText}>Rp. 1.000.000</Text>
            </View> */}
          {/* </View> */}
          <View style={styles.cardAddDebt}>
            <Button
                icon="md-add-circle-outline"
                label="Add Debt"
                // onPress={() => WebBrowser.openBrowserAsync('https://docs.expo.io')}
              />
          </View>
        </View>
      </ScrollView>

    </View>
  );
}

function Button({ icon, label, onPress, isLastOption }) {
  return (
    <RectButton style={[styles.option, isLastOption && styles.lastOption]} onPress={onPress}>
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.optionIconContainer}>
          <Ionicons name={icon} size={30} color="rgba(46,139,87,0.80)" />
        </View>
        <View style={styles.optionTextContainer}>
          <Text style={styles.optionText}>{label}</Text>
        </View>
      </View>
    </RectButton>
  );
}

Wallet.navigationOptions = {
  header: null,
};

function DevelopmentModeNotice() {
  if (__DEV__) {
    const learnMoreButton = (
      <Text onPress={handleLearnMorePress} style={styles.helpLinkText}>
        Learn more
      </Text>
    );

    return (
      <Text style={styles.developmentModeText}>
        Development mode is enabled: your app will be slower but you can use useful development
        tools. {learnMoreButton}
      </Text>
    );
  } else {
    return (
      <Text style={styles.developmentModeText}>
        You are not in development mode: your app will run at full speed.
      </Text>
    );
  }
}

function handleLearnMorePress() {
  WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/workflow/development-mode/');
}

function handleHelpPress() {
  WebBrowser.openBrowserAsync(
    'https://docs.expo.io/versions/latest/get-started/create-a-new-app/#making-your-first-change'
  );
}