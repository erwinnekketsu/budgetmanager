import { Platform, StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    developmentModeText: {
        marginBottom: 20,
        color: 'rgba(0,0,0,0.4)',
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 5,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    option: {
        height: 50,
        margin: 2,
        marginTop: 10,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 10,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: 'seagreen',
      },
    optionIconContainer: {
        marginRight: 10,
    },
    lastOption: {
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    optionText: {
        fontSize: 15,
        alignSelf: 'flex-start',
        marginTop: 5,
        color: 'seagreen',
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
    },
    homeFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        lineHeight: 24,
        textAlign: 'center',
    },
    tabBarInfoContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        ...Platform.select({
        ios: {
            shadowColor: 'black',
            shadowOffset: { width: 0, height: -3 },
            shadowOpacity: 0.1,
            shadowRadius: 3,
        },
            android: {
            elevation: 20,
            },
        }),
        alignItems: 'center',
        backgroundColor: '#fbfbfb',
        paddingVertical: 20,
    },
    tabBarInfoText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
    },
    navigationFilename: {
        marginTop: 5,
    },
    helpContainer: {
        marginTop: 15,
        alignItems: 'center',
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
    card: {
        height: 70,
        backgroundColor: '#fff',
        margin: 5,
        marginTop: 20,
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
        flexDirection: 'row',
    },
    cardDivLeft: {
        width: '50%',
        borderRightWidth: 1,
        borderRightColor: 'seagreen',
    },
    cardDivRight: {
        width: '50%'
    },
    cardDivRightIncome: {
        // height: '50%',
    },
    cardTextDivLeft: {
        color: 'seagreen',
        textAlign: 'left',
        marginTop: 10,
        marginLeft: 5
    },
    cardTextDivRight: {
        color: 'seagreen',
        textAlign: 'left',
        // marginTop: 7,
        marginLeft: 5
    },
    cardTextLeft: {
        color: 'seagreen',
        textAlign: 'left',
        // marginTop: 5,
        marginLeft: 5,
        fontSize: 20,
    },
    cardTextRight: {
        color: 'seagreen',
        textAlign: 'left',
        marginTop: 5,
        marginLeft: 5,
        fontSize: 20,
    },
    //scroll view begin
    cardList: {
        backgroundColor: '#fff',
        margin: 5,
        // borderWidth: 1,
        // borderColor: 'seagreen',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
    },
    cardListHeader: {
        height: 50,
        flexDirection: 'row',
    },
    cardListDate: {
        width: '20%',
        margin: 'auto',
    },
    cardListDateText: {
        fontSize: 30,
        fontWeight: "bold",
        textAlign: 'center',
        marginTop: 5,
    },
    cardListDay: {
        width: '30%',
        paddingTop: 8,
    },
    cardListDayTextTop: {
        fontSize: 15,
        fontWeight: "bold",
        textAlign: 'left',
    },
    cardListDayTexBottom: {
        fontSize: 10,
        textAlign: 'left',
    },
    cardListDayTotal: {
        width: '50%',
    },
    cardListDayTotalText: {
        fontSize: 15,
        fontWeight: "bold",
        textAlign: 'right',
        marginRight: 10,
        marginTop: 15,
    },
    cardListBudget: {
        flexDirection: 'row',
    },
    cardListBudgetIcon: {
        width: '20%',
        margin: 'auto',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardListBudgetIconImage: {
        marginTop: 5,
        borderRadius: 400/ 2,
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },
    cardListBudgetTrx: {
        width: '30%',
        paddingTop: 8,
    },
    cardListBudgetPrice: {
        width: '50%',
    },
    cardAddBudget: {
        
    },
    cardSaving: {
        backgroundColor: '#fff',
        margin: 5,
        marginTop: 10,
        // borderWidth: 1,
        // borderColor: 'seagreen',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
    },
    cardListSaving: {
        marginTop: 10,
        flexDirection: 'row',
    },
    cardListSavingIcon: {
        width: '20%',
        margin: 'auto',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardListSavingIconImage: {
        marginTop: 5,
        borderRadius: 400/ 2,
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },
    cardListSavingTrx: {
        width: '30%',
        paddingTop: 8,
    },
    cardListSavingPrice: {
        width: '50%',
    },
    cardAddSaving: {
        
    },
    cardDebt: {
        backgroundColor: '#fff',
        margin: 5,
        marginTop: 10,
        // borderWidth: 1,
        // borderColor: 'seagreen',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
    },
    cardListDebt: {
        marginTop: 10,
        flexDirection: 'row',
    },
    cardListDebtIcon: {
        width: '20%',
        margin: 'auto',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardListDebtIconImage: {
        marginTop: 5,
        borderRadius: 400/ 2,
        width: 50,
        height: 50,
        resizeMode: 'contain',
    },
    cardListDebtTrx: {
        width: '30%',
        paddingTop: 8,
    },
    cardListDebtPrice: {
        width: '50%',
    },
    cardAddDebt: {
        
    }
    //scroll view begin
  });